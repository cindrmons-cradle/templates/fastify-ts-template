import fastify from "fastify";

const server = fastify();

server.get('/ping',async (req, reply) => {
    return 'pong!\n';
});

server.listen({port: 9317}, (err, addr) => {
    if (err) {
        console.error(err)
        process.exit(1)
    }

    console.log(`Server is now active at ${addr}`)
    console.log("Press Ctrl+C to Exit.")
})
